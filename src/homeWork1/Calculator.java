package homeWork1;

public class Calculator {
    public static void main(String[] args) {
        int firstNumber = 10; //first variable
        int secondNumber = 5; //second variable
        /*
        The next part of the code is used for calling the methods.
        These methods are taken from different classes
         */
        int adding = Plus.plus(firstNumber, secondNumber);
        int sub = Minus.minus(firstNumber, secondNumber);
        int div = Divide.division(firstNumber, secondNumber);
        int mul = Multiply.multiply(firstNumber, secondNumber);
        //Displaying the results
        System.out.println(adding);
        System.out.println(sub);
        System.out.println(div);
        System.out.println(mul);
/**
 * Java supports the usual logical conditions from mathematics:
 * Less than: a < b
 * Less than or equal to: a <= b
 * Greater than: a > b
 * Greater than or equal to: a >= b
 * Equal to a == b
 * Not Equal to: a != b
 * You can use these conditions to perform different actions for different decisions.
 */
        if (firstNumber == secondNumber) {
            System.out.println("Sum of " + firstNumber + " and " + secondNumber + " = "
                    + Double.sum(firstNumber, secondNumber));
        } else {
            int adding1 = (firstNumber + secondNumber);
            System.out.println("The sum of numbers is " + adding1);

        }

        int a = 8;
        int z = 8;
        int b = z++;
        int c = ++a;
        System.out.println(b + " first; " + c + " second ");
    }
}

